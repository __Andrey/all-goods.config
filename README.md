# all-goods.config

```sh
ansible-playbook vpn.yml              -i hosts.ini 

ansible-playbook sentry.yml           -i hosts.ini  -e @sentry.vars.yml

ansible-playbook all-goods.back.yml   -i hosts.ini  -e @all-goods.back.vars.yml
ansible-playbook all-goods.front.yml  -i hosts.ini  -e @all-goods.front.vars.yml

ansible-playbook db_create_dump.yml   -i hosts.ini  -e @zig.staging.back.vars.yml
ansible-playbook db_restore_dump.yml  -i hosts.ini  -e @walkie.back.vars.yml --extra-vars "dump_name=zig-back"
```
```sh

less /home/all-goods/all-goods.back/celerybeat.log
less /home/all-goods/all-goods.back/celery-celery1.log
less /home/all-goods/all-goods.back/celery-celery2.log
less /home/all-goods/all-goods.back/celery-celery3.log
less /home/all-goods/all-goods.back/celery-celery4.log

rm /home/all-goods/all-goods.back/celerybeat.log
rm /home/all-goods/all-goods.back/celery-celery*.log
rm /home/all-goods/all-goods.back/celery-worker*.log
ls -al /home/all-goods/all-goods.back/
```