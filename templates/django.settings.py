DEBUG = True
DATABASES = {
    'default': {
        'ENGINE'    : 'django.db.backends.postgresql',
        'NAME'      : '{{db.name}}',
        'USER'      : '{{db.user}}',
        'PASSWORD'  : '{{db.password}}',
        'HOST'      : '127.0.0.1',
        'PORT'      : '5432',
    }
}

CORS_ORIGIN_ALLOW_ALL = True
ALLOWED_HOSTS = [ '*', ]
STATIC_ROOT = '{{ project.dir }}/static'
