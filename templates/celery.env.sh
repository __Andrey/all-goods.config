
# Names of nodes to start
#   most people will only start one node:
# CELERYD_NODES="worker1"
#   but you can also start multiple and configure settings
#   for each in CELERYD_OPTS
# CELERYD_NODES="worker1 worker2 worker3"
#   alternatively, you can specify the number of nodes to start:
CELERYD_NODES=4

# Absolute or relative path to the 'celery' command:
CELERY_BIN="{{ project.dir }}/.env/bin/celery"
#CELERY_BIN="/virtualenvs/def/bin/celery"

# App instance to use
# comment out this line if you don't use an app
# CELERY_APP="proj"
# or fully qualified:
#CELERY_APP="proj.tasks:app"
# CELERY_APP="{{ project.name }}"
# `main` is a folder inside the project
CELERY_APP="main"

# Where to chdir at start.
CELERYD_CHDIR="{{ project.dir }}/src"

# Extra command-line arguments to the worker
CELERYD_OPTS="--time-limit=300 --concurrency=2"
# Configure node-specific settings by appending node name to arguments:
#CELERYD_OPTS="--time-limit=300 -c 8 -c:worker2 4 -c:worker3 2 -Ofair:worker1"

# %n will be replaced with the first part of the nodename.
# CELERYD_LOG_FILE="/var/log/celery/%n%I.log"
# CELERYD_PID_FILE="/var/run/celery/%n.pid"
CELERYD_LOG_LEVEL="INFO"
CELERYD_LOG_FILE="{{ project.dir }}/celery-%n.log"
CELERYD_PID_FILE="{{ project.dir }}/celery-%N.pid"

# Workers should run as an unprivileged user.
#   You need to create this user manually (or you can choose
#   a user/group combination that already exists (e.g., nobody).
# CELERYD_USER="celery"
# CELERYD_GROUP="celery"
CELERYD_USER="{{ user }}"
CELERYD_GROUP="{{ user }}"

# If enabled pid and log directories will be created if missing,
# and owned by the userid/group configured.
CELERY_CREATE_DIRS=1


# ENABLED="true"
# CELERY_CONFIG_MODULE="celeryd"
# How to call manage.py
# CELERYD_MULTI="multi"

CELERYBEAT_LOG_LEVEL="INFO"
CELERYBEAT_LOG_FILE="{{ project.dir }}/celerybeat.log"
CELERYBEAT_PID_FILE="{{ project.dir }}/celerybeat.pid"
# Scheduler for celery
CELERYBEAT_OPTS="--pidfile={{ project.dir }}/celerybeat.pid  --scheduler=django_celery_beat.schedulers:DatabaseScheduler"
